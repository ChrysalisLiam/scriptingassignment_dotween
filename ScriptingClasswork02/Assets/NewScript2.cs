﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NewScript2 : MonoBehaviour
{
    [SerializeField] private bool movingRight = true;
    [SerializeField] private bool moving = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bouncer")
        {
            Bounce();
        }
    }

    void Start()
    {
        Bounce();
    }

    private void Bounce()
    {
        transform.localScale = Vector3.one;

        moving = false;
        movingRight = !movingRight;
        transform.DOPunchScale(new Vector3(0.3f, 0.3f, 0.3f), 1f, 5);

        if (movingRight)
        {
            if (!moving)
            {
                transform.DOMoveX(10f, 2.0f);
                moving = true;
            }
        }
        else
        {
            if (!moving)
            {
                transform.DOMoveX(-10f, 2.0f);
                moving = true;
            }
        }
    }
}
