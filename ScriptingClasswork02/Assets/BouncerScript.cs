﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BouncerScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            Bounce();
        }
    }

    private void Bounce()
    {
        transform.DOPunchScale(new Vector3(0f, 0.5f, 0f), 1f, 5);
    }
}
