﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WobbleScript : MonoBehaviour
{
    [SerializeField] private float delay = 0f;

    private void Start()
    {
        Invoke("Wobble", delay);
    }

    private void Wobble()
    {
        transform.DOPunchScale(new Vector3(0.3f, 0.3f, 0f), 3f, 9).OnComplete(Wobble);
    }

}
